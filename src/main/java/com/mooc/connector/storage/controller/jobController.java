package com.mooc.connector.storage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mooc.connector.storage.application.StorageConnectionApplicationService;
import com.mooc.connector.storage.application.Model.CourseImportStatusDTO;
import com.mooc.connector.storage.infrastructure.repository.MycolMoocTestSolrRepository;

@RestController
public class jobController {

	@Autowired
	StorageConnectionApplicationService storageConnectionApplicationService;

	@Autowired
	MycolMoocTestSolrRepository mycolMoocTestSolrRepository;

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/start-job", method = RequestMethod.GET)
	public CourseImportStatusDTO startJob(@RequestParam(name = "import-id") String importId) {

		return storageConnectionApplicationService.startJob(importId);
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public CourseImportStatusDTO  getStatus(@RequestParam(name = "import-id") String importId) {

		return storageConnectionApplicationService.getImportStatus(importId);
	}
}
