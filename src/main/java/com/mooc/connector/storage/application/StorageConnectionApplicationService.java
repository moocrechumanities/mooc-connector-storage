package com.mooc.connector.storage.application;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mooc.connector.storage.application.Model.CourseImportStatusDTO;
import com.mooc.connector.storage.application.transformer.StorageTransformer;
import com.mooc.connector.storage.domain.StorageServiceImpl;

import ch.qos.logback.classic.Logger;

@Service
public class StorageConnectionApplicationService {

	private Logger logger = (Logger) LoggerFactory.getLogger(StorageConnectionApplicationService.class);

	@Autowired
	StorageServiceImpl storageServiceImpl;

	@Autowired
	StorageTransformer storageTransformer;

	public CourseImportStatusDTO startJob(String importId) {

		logger.info("STARTING ASYNC STORAGE SERVICE");

		CompletableFuture.runAsync(() -> {
			try {

				storageServiceImpl.asyncStoreIntoStorage(importId);

			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		});

		return storageTransformer.toCourseImportProgressStatusResopnse(importId);
	}

	public CourseImportStatusDTO getImportStatus(String importId) {

		return storageServiceImpl.getImportStatusFromDb(importId);
	}

}
