package com.mooc.connector.storage.application.DTO.material;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MaterialResponseDTO {

	private MaterialPresentageDTO pdf;
	private MaterialPresentageDTO quiz;

	public MaterialPresentageDTO getPdf() {
		return pdf;
	}

	public void setPdf(MaterialPresentageDTO pdf) {
		this.pdf = pdf;
	}

	public MaterialPresentageDTO getQuiz() {
		return quiz;
	}

	public void setQuiz(MaterialPresentageDTO quiz) {
		this.quiz = quiz;
	}

}
