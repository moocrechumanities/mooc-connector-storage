package com.mooc.connector.storage.application.transformer;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mooc.connector.storage.application.DTO.material.MaterialResponseDTO;
import com.mooc.connector.storage.application.DTO.video.VideoResponseDTO;
import com.mooc.connector.storage.application.Model.CourseImportStatusDTO;
import com.mooc.connector.storage.application.Model.CourseModel;
import com.mooc.connector.storage.infrastructure.repository.CourseImportStatusRepository;

import ch.qos.logback.classic.Logger;

@Service
public class StorageTransformer {

	private Logger logger = (Logger) LoggerFactory.getLogger(StorageTransformer.class);

	@Autowired
	CourseImportStatusRepository courseImportStatusRepository;

	private Double randID = getRandomDoubleBetweenRange(0.0, 10000000000.0);

	public CourseImportStatusDTO toCourseImportProgressStatusResopnse(String importId) {

		logger.info("COURSE IMPORT PROGRESS STATUS RESPONSE");

		CourseImportStatusDTO courseImportStatusDTO = new CourseImportStatusDTO();

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();

		courseImportStatusDTO.setDate(formatter.format(date));
		courseImportStatusDTO.setStatus("INPROGRESS");
		courseImportStatusDTO.setImportId(importId);

		courseImportStatusRepository.save(courseImportStatusDTO);

		return courseImportStatusDTO;
	}

	public CourseImportStatusDTO toCourseImportCompleteProgressStatusResopnse(String importId) {

		logger.info("IMPORT COMPLETE PRGRESS STATUS RESPONSE");

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();

		CourseImportStatusDTO courseImportStatusDTO = courseImportStatusRepository.getImportStatusById(importId);

		courseImportStatusDTO.setDate(formatter.format(date));
		courseImportStatusDTO.setStatus("COMPLETED");

		courseImportStatusRepository.save(courseImportStatusDTO);

		return courseImportStatusDTO;
	}

	public CourseImportStatusDTO toCourseImportErrorProgressStatusResopnse(String importId) {

		logger.info("IMPORT ERROR PRGRESS STATUS RESPONSE");

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();

		CourseImportStatusDTO courseImportStatusDTO = courseImportStatusRepository.getImportStatusById(importId);

		courseImportStatusDTO.setDate(formatter.format(date));
		courseImportStatusDTO.setStatus("ERROR");

		courseImportStatusRepository.save(courseImportStatusDTO);

		return courseImportStatusDTO;
	}

	public CourseModel toCourseModel(MaterialResponseDTO material, VideoResponseDTO video, String learnerStyle) {

		CourseModel courseModel = new CourseModel();

		courseModel.setId("C_002");
		courseModel.setAccent("en_US");
		courseModel.setSource("coursera");
		courseModel.setUrl("https://www.coursera.org/learn/terrorism");
		courseModel.setPdfWeight(Integer.toString(material.getPdf().getPercentage()));
		courseModel.setQuizWeight(Integer.toString(material.getQuiz().getPercentage()));
		courseModel.setLearnerStyle(learnerStyle);

		return courseModel;
	}

	private double getRandomDoubleBetweenRange(double min, double max) {
		return (Math.random() * ((max - min) + 1)) + min;

	}

}
