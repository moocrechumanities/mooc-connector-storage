package com.mooc.connector.storage.application.Model;

import java.util.List;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

@SolrDocument(collection = "mooc")
public class MyColMoocTestSolrModel {

	@Id
	private String id;
	private String courseId;
	private String title;
	private String content;
	private String type;
	private String courseSource;
	@Field(child = true)
	private List<SubModuleSolrModel> subModule;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCourseSource() {
		return courseSource;
	}
	public void setCourseSource(String courseSource) {
		this.courseSource = courseSource;
	}
	public List<SubModuleSolrModel> getSubModule() {
		return subModule;
	}
	public void setSubModule(List<SubModuleSolrModel> subModule) {
		this.subModule = subModule;
	}
	
}
