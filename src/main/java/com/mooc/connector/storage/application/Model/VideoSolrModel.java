package com.mooc.connector.storage.application.Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

@SolrDocument(collection = "mycol-mooc-test")
public class VideoSolrModel {

	@Id
	private String id;
	private String courseId;
	private String title;
	private String content;
	private String type;
	private String courseSource;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCourseSource() {
		return courseSource;
	}

	public void setCourseSource(String courseSource) {
		this.courseSource = courseSource;
	}

}
