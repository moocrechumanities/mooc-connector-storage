package com.mooc.connector.storage.application.Model;

import java.util.List;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

@SolrDocument(collection = "mycol-mooc-test")
public class SubModuleSolrModel {
	@Id
	private String id;
	private String courseId;
	private String title;
	private String content;
	private String type;
	private String courseSource;
	@Field(child = true)
	private List<VideoSolrModel> video;

	@Field(child = true)
	private List<ReadingSolrModel> reading;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCourseSource() {
		return courseSource;
	}

	public void setCourseSource(String courseSource) {
		this.courseSource = courseSource;
	}

	public List<VideoSolrModel> getVideo() {
		return video;
	}

	public void setVideo(List<VideoSolrModel> video) {
		this.video = video;
	}

	public List<ReadingSolrModel> getReading() {
		return reading;
	}

	public void setReading(List<ReadingSolrModel> reading) {
		this.reading = reading;
	}

}
