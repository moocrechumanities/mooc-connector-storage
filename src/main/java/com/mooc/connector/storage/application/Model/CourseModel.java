package com.mooc.connector.storage.application.Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "courses")
public class CourseModel {

	@Id
	private String _id;
	private String id;
	private String title;
	private String learnerStyle;
	private String quizWeight;
	private String pdfWeight;
	private String accent;
	private String source;
	private String url;

	public String get_id() {
		return _id;
	}

	public String getQuizWeight() {
		return quizWeight;
	}

	public void setQuizWeight(String quizWeight) {
		this.quizWeight = quizWeight;
	}

	public String getPdfWeight() {
		return pdfWeight;
	}

	public void setPdfWeight(String pdfWeight) {
		this.pdfWeight = pdfWeight;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLearnerStyle() {
		return learnerStyle;
	}

	public void setLearnerStyle(String learnerStyle) {
		this.learnerStyle = learnerStyle;
	}

	public String getAccent() {
		return accent;
	}

	public void setAccent(String accent) {
		this.accent = accent;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public CourseModel() {

		this.id = "ZC_001";
		this.title = "Terrorism";
		this.learnerStyle = "VR";
		this.accent = "en_US";
		this.source = "coursera";
		this.url = "https://www.coursera.org/learn/terrorism";
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
