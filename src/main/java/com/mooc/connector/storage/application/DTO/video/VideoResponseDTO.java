package com.mooc.connector.storage.application.DTO.video;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VideoResponseDTO {

	private double conversation;
	private double person;
	private double text;
	
	public double getConversation() {
		return conversation;
	}

	public void setConversation(double conversation) {
		this.conversation = conversation;
	}

	public double getPerson() {
		return person;
	}

	public void setPerson(double person) {
		this.person = person;
	}

	public double getText() {
		return text;
	}

	public void setText(double text) {
		this.text = text;
	}


}
