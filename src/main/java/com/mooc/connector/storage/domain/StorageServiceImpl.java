package com.mooc.connector.storage.domain;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mooc.connector.storage.application.DTO.material.MaterialResponseDTO;
import com.mooc.connector.storage.application.DTO.video.VideoResponseDTO;
import com.mooc.connector.storage.application.Model.CourseImportStatusDTO;
import com.mooc.connector.storage.application.Model.CourseModel;
import com.mooc.connector.storage.application.Model.MyColMoocTestSolrModel;
import com.mooc.connector.storage.application.transformer.StorageTransformer;

import com.mooc.connector.storage.infrastructure.repository.CourseRepository;
import com.mooc.connector.storage.infrastructure.repository.MycolMoocTestSolrRepository;
import com.mooc.connector.storage.infrastructure.repository.CourseImportStatusRepository;

import ch.qos.logback.classic.Logger;

@Service
public class StorageServiceImpl implements StorageService {

	private Logger logger = (Logger) LoggerFactory.getLogger(StorageServiceImpl.class);

	@Autowired
	StorageTransformer storageTransformer;

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	MycolMoocTestSolrRepository mycolMoocTestSolrRepository;

	@Autowired
	MaterialClassificationIntegrationService materialClassificationIntegrationService;

	@Autowired
	VideoClassificationIntegrationService videoClassificationIntegrationService;

	@Autowired
	CourseImportStatusRepository courseImportStatusRepository;
	
	@Override
	public CourseImportStatusDTO asyncStoreIntoStorage(String importId) throws InterruptedException {

		logger.info("ASYNC IMPORT STARTED");
		
		try {
			
			MaterialResponseDTO material = materialClassificationIntegrationService.getResultfromMaterialClassificationService();
			VideoResponseDTO video = videoClassificationIntegrationService.getResultfromVideoClassificationService();

			String learnerStyle = learnerStyleValueCaluculation(video);
			CourseModel courseModel = storageTransformer.toCourseModel(material, video, learnerStyle);

			storeInMongo(courseModel);
//			storeInSolr();
			
			return storageTransformer.toCourseImportCompleteProgressStatusResopnse(importId);
			
		}catch(Exception e) {
			
			return storageTransformer.toCourseImportErrorProgressStatusResopnse(importId);
		}
		
	}

	@Override
	public void storeInMongo(CourseModel courseModel) throws InterruptedException {

		logger.info("JSON :" + courseModel);

		courseRepository.save(courseModel);
		Thread.sleep(5000);

	}

	@Override
	public void storeInSolr() {

		MyColMoocTestSolrModel courseModel = new MyColMoocTestSolrModel();
		try {

			mycolMoocTestSolrRepository.save(courseModel);

		} catch (Exception e) {
			logger.info("SOLR ENGINE HAS SOME PROBLEM");
		}
	}

	private String learnerStyleValueCaluculation(VideoResponseDTO video) {

		if (video.getConversation() >= 28 && video.getText() >= 28 && video.getPerson() >= 28) {
			
			 return "VAR";

		} else if (video.getText() > video.getConversation() || video.getText() > video.getPerson()) {
			 
			 if(video.getConversation() > video.getText() || video.getPerson() > video.getText()) {
				 
				 return "VAR";
				 
			 }else {
				 
				 return "VR";
			 }

		} else if (video.getConversation() > video.getText() || video.getPerson() > video.getText()) {
		     
		     if(video.getText() > video.getConversation() || video.getText() > video.getPerson()) {
		    	 
		    	 return "VAR";
		    	 
		     }else {
		    	 
		    	 return "A";
		     }

		}else {
			return null; 
		}

	}

	public CourseImportStatusDTO getImportStatusFromDb(String importId) {
		
		return courseImportStatusRepository.getImportStatusById(importId);
	}

	@Override
	public CourseImportStatusDTO asyncStoreIntoStorage() throws InterruptedException {
		// TODO Auto-generated method stub
		return null;
	}

}
