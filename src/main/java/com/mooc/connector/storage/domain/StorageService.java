package com.mooc.connector.storage.domain;

import com.mooc.connector.storage.application.DTO.material.MaterialResponseDTO;
import com.mooc.connector.storage.application.DTO.video.VideoResponseDTO;
import com.mooc.connector.storage.application.Model.CourseImportStatusDTO;
import com.mooc.connector.storage.application.Model.CourseModel;

public interface StorageService {

	public CourseImportStatusDTO asyncStoreIntoStorage() throws InterruptedException;
	public void storeInMongo(CourseModel courseModel) throws InterruptedException;
	public void storeInSolr();
	public CourseImportStatusDTO asyncStoreIntoStorage(String importId) throws InterruptedException;

}
