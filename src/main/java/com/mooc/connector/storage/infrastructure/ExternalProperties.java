package com.mooc.connector.storage.infrastructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ExternalProperties {

	@Value("${aws.base.public.url}")
	private String awsBaseUrl;

	@Value("${material.classification.uri}")
	private String materialUri;

	@Value("${video.classification.uri}")
	private String videoUri;

	public String getAwsBaseUrl() {
		return awsBaseUrl;
	}

	public void setAwsBaseUrl(String awsBaseUrl) {
		this.awsBaseUrl = awsBaseUrl;
	}

	public String getMaterialUri() {
		return materialUri;
	}

	public void setMaterialUri(String materialUri) {
		this.materialUri = materialUri;
	}

	public String getVideoUri() {
		return videoUri;
	}

	public void setVideoUri(String videoUri) {
		this.videoUri = videoUri;
	}

}
