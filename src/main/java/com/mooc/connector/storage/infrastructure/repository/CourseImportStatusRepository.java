package com.mooc.connector.storage.infrastructure.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.mooc.connector.storage.application.Model.CourseImportStatusDTO;

public interface CourseImportStatusRepository extends MongoRepository<CourseImportStatusDTO, String> {

	@Query("{ importId : ?0 }")
	public CourseImportStatusDTO getImportStatusById(String id);
	
}