package com.mooc.connector.storage.infrastructure.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mooc.connector.storage.application.Model.CourseModel;

public interface CourseRepository extends MongoRepository<CourseModel, String> {

}
