package com.mooc.connector.storage.infrastructure.repository;

import org.springframework.data.solr.repository.SolrCrudRepository;

import com.mooc.connector.storage.application.Model.MyColMoocTestSolrModel;

public interface MycolMoocTestSolrRepository extends SolrCrudRepository<MyColMoocTestSolrModel, String> {

}
