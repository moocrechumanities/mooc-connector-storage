package com.mooc.connector.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoocConnectorStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoocConnectorStorageApplication.class, args);
	}

}
